---
title: Deploying Your Site
date: 2019-03-17T03:25:35.475Z
published: true
author: Kendall Roth
description: >-
  Even awesome websites are meaningless if they are only running locally on a
  developer's laptop! This post looks at deploying to a static site provider and
  using a content-based CMS to manage site data.
tags:
  - Project
  - Setup
  - Domain
---
## Selecting a Static Site Provider

There are several common static site providers that offer well-rounded basic (and **free** plans), including [Netlify](https://www.netlify.com), AWS S3, Zeit, and Now.sh. Having used Netlify on previous project, I chose to keep my configuration all in one dashboard. Netlify offers an all-in-one workflow combining global deployment, continuous integration with your Git repository, and automatic SSL certificates (enabling HTTPS). Their workflow is a breeze to setup &ndash; connect it to a GitLab repository and Netlify will automatically create the pipelines necessary to deploy your site whenever the deploy branch changes (`master` by default).

![Netlify Workflow](/images/blog-2019-03-17-netlify.png)

## Configuring Netlify

A new project can be created easily by selecting "New Site from Git" in the Netlify Dashboard, which will prompt the user to select a repository from the Git provider. After confirming (or changing) the default deployment options, Netlify will setup continuous deployment and even publish the initial site.

### Setup Your Domain

By default, the site will be published as a (customizable) Netlify subdomain, similar to `family-merchant.netlify.com`. While this works, it is much more meaningful to connect it to the domain purchased initially for the site. To access your site on a non-Netlify domain name, add a custom domain through the "Domain Management" settings page.

I've included my setup as an example below; notice that the primary domain includes the `www` prefix. This is done to follow recommended SEO practices, with the "bare" domain redirecting to avoid being indexed separately! Netlify will automatically configure this for you when specifying your initial custom domain.

```
# Default subdomain
family-merchant.netlify.com

# Primary domain
www.family-merchant.com

# Redirects automatically to primary domain
family-merchant.com
```

### Verify Your Domain

Your domain must be verified before Netlify can redirect traffic from its default subdomain. This verification process involves creating several DNS records in the DNS provider you chose previously (Digital Ocean for me). Traffic will be redirected from your domain to the nameservers you specified, and then through the new DNS records to Netlify.

> Using Netlify for DNS management will enable several additional features, including deploying branches to their own subdomain (ie. `dev` branch to `dev.family-merchant.com`). However, I avoided this because I wanted to keep all of my DNS configuration in Digital Ocean.

In Digital Ocean, add two records in the "Networking" tab for this site. The `CNAME` record (or alias record) will be used to direct traffic to the Netlify-generated subdomain, which will in turn redirect to your primary custom domain in Netlify. The `A` record (or host record) will point to the Netlify nameservers.

![Digital Ocean DNS](/images/blog-2019-03-17-digital-ocean-dns.png)


> Use very low TTL values (ex. `60s`) when adding DNS records in development, to ensure a rapid turn-around time if you make a mistake. These values can be increased once you have finalized your configuration; just remember that it may take that long for future change to propagate!

It may take a while for these changes to propagate (up to 24 hours, but unlikely). Once the changes have propagated globally, Netlify will verify your custom domain and start redirecting traffic from their default subdomain to your custom domain. At this point you should be able to visit your domain address and see your site being served globally!

### Enable SSL

Once your domain has been verified, enable SSL in your site's Netlify Dashboard. This may take some time, but will eventually make your app accessible with the `HTTPS` protocol (including all current subdomains). Netlify uses Let's Encrypt to provide the free SSL certificates; feel free to donate to their cause if you wish to support continual free certificates!

> Do not enable SSL until all desired records are configured in Netlify, as it will only create the SSL certificate for the current records.

### Enable Redirects

Although Netlify handles a basic redirect from the bare domain to the `www` subdomain, there are many other potential problems with the URL configuration currently. For example, the non-HTTPS route should redirect to the HTTPS route, the Netlify subdomain should redirect to the primary domain, etc.

Fortunately, Netlify provides a way to customize the routing and redirections through the use of a `_redirects` file, placed at the root of the deployed project (ie. in `static` directory). The sample file shows several examples, including some redundant ones handled by the default Netlify redirection (bare subdomain to "www").

With each redirection rule an HTTP status code can be defined (permanent redirection used here). Additionally, the route paths can also be passed through the redirection using the `:splat` operator in correlation with the `*` in the rule target. This will pass whatever path follows the base URL through the redirection, instead of losing the path on the way. For example, `http://family-merchant.com/blog/` will be redirected to `https://www.family-merchant.com/blog/` instead of losing the `/blog/` path (super important for a good experience!).

```
# Redirect http to https
http://family-merchant.com/*           https://family-merchant.com/:splat      301!
http://www.family-merchant.com/*       https://www.family-merchant.com/:splat  301!

# Redirect from Netlify subdomain to actual app subdomain
https://family-merchant.netlify.com/*  https://family-merchant.com/:splat      301!

# Redirect from "bare" domain to "www" subdomain
https://family-merchant.com/*          https://www.family-merchant.com/:splat  301!
```

## Manage Your Content

_Coming Soon_
