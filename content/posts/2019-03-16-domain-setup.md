---
title: Domain Setup
date: 2019-03-17T00:54:57.151Z
published: true
author: Kendall Roth
description: >-
  A successful game requires interest and hype before it is ever released. An
  easy way to generate hype is through releasing tidbits leading up to the
  release date, whether through social media or game website. In this post I'll
  go through the process of setting up a simple website using a JAMstack.
tags:
  - Setup
  - Domain
  - Project
---
## Introduction

Setting up a website can be challenging for anyone, even those who have done it many times before. The reason for this is simple - the web development world is in a state of **constant** flux as technologies gain and lose favour. One of the most popular of late is a loose framework called the [JAMstack](https://www.jamstack.org/). Based on JavaScript, APIs, and Markdown, the JAMstack offers a fast pre-built alternative to traditional SPAs (single page applications). Moving the build process to the server, rather than the browser, results in faster/better experience using the site.

For this website I choose to use the [VueJS](https://www.vuejs.org) UI framework, a popular competitor to React. The site is compiled and built with [Gridsome](https://www.gridsome.org), an up-and-coming static site generator for Vue (similar to Gatsby for React). Finally, the compiled site is served with [Netlify](https://www.netlify.com), an easy-to-use static site host (_most importantly, with a free plan and SSL certificates_).

## Domain

However, even before any of that can discussed (or at least _implemented_), a domain must be purchased to eventually host the site. My domain name provider of choice is [NameCheap](https://www.namecheap.com), but there are many viable and popular alternatives. I've always been impressed with NameCheap's user interface and the domain purchase process, but again, there are many similar alternatives.

I purchased both `family-merchant.com` and `family-merchant.ca` (being a resident of Ontario, Canada). Obviously, maintaining two different sites would be a massive hassle, I redirected all traffic on the `family-merchant.ca` domain to the `family-merchant.com` domain.

> Purchasing multiple top-level domains (the `.com` and `.ca`) is unnecessary but does ensure that no one else can "poach" your domain and pretend to represent you with an "official" domain.

### Domain Redirection

NameCheap offers a handy feature, called "URL Redirection," that can be configured to perform this redirection without over-complicating the process. The domain dashboard (for the domain you wish to redirect traffic **away** from) offers a "Redirect Domain" list, where you can specify specific URLs to redirect **or** a wildcard that will match all unlisted subdomains.

```
# Redirect Domain (family-merchant.ca)

# Will redirect any requests to "family-merchant.ca" to the proper ".com" site
family-merchant.ca    --> https://www.family-merchant.com
# Will redirect any requests to "[SUBDOMAIN].family-merchant.com" to the proper ".com" site
Wildcard Redirect     --> https://www.family-merchant.com
```

> This URL redirection will not preserve any path after the target redirected link. For example, `family-merchant.ca/blog` would be redirected to `family-merchant.com` (no `/blog` path).

### Configure Nameservers

Once any other domains (if any) have been properly redirected to the main domain, it is time to configure the DNS for the main domain. This can be handled either by the domain provider (ie. NameCheap) or by another 3rd party DNS service. I use [DigitalOcean](https://www.digitalocean.com) for other web hosting needs and like its DNS management interface, but other alternatives do exist (using the domain provider itself is often easiest).

If an external DNS service is chosen (not the domain provider), the domain nameservers must be pointed to the target DNS service nameservers. The NameCheap domain dashboard offers the "Nameservers" feature that accepts a list of nameservers to use for custom DNS records. After selecting "Custom DNS," enter the nameserver addresses provided by the selected DNS service.

```
# DigitalOcean nameservers
ns1.digitalocean.com
ns2.digitalocean.com
ns3.digitalocean.com
```

At this point there is no need to add any DNS records in the selected service, as there is currently nowhere to point the domain (app has not yet been hosted). This will be discussed in the next blog post, along with selecting a static site generator.
