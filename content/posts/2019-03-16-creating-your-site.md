---
title: Creating Your Site
date: 2019-03-17T01:46:05.016Z
published: true
author: Kendall Roth
description: >-
  The first decision in creating any site is what technologies to use, will
  plain HTML work or is a templating framework a better choice? This post will
  describe several options available and why I chose to work with Vue, a UI
  templating framework with an HTML feel.
tags:
  - Setup
  - Project
---
## Selecting Technologies

There are **way** too many options available when trying to determine how you will build your website. To narrow it down, I chose to ignore the drag-and-drop style common with many online website builders. While these do serve a purpose for many people, it is not suited to those who want full control over their site. Plus, it's typically quite pricey for a simple site and a beginning developer's budget. Additionally, I decided to use a static site generator rather than a "traditional" SPA (ie. "vanilla" React, Vue, etc) or isomorphic app (ie. Next, Nuxt, etc), but still use a JS-based framework.

### Static Site Generators

Static site generators vary from "traditional" SPAs by generating an _entirely static_ website from source files, rather than building the site from compiled files at run time. The compiled assets are served individually, rather than as a whole in traditional SPAs, resulting in smaller bundle sizes by default. Serving a static site is extremely fast because all the web server needs to do is return a single small file, rather than the same larger file for all routes. Traditional SPAs and other server side approaches must build the whole page from scratch, whether on the server or the client.

Additional benefits include increased security (only serving static files), improved scalability (simply distribute static files to more locations), and managing the entire project through version control (if using Markdown to store content).

Within this category were two main competitors for me to choose between, the very popular [Gatsby](https://www.gatsbyjs.org) and the newcomer [Gridsome](https://gridsome.org).

> Other popular alternatives, including Jekyll, Nugo, and Middleman, were excluded as they used their own templating language **other** than a JS-based approach.

### Gatsby

Gatsby has been around for quite some time and has garnered a significant amount of interest, evidenced by its over `32000` stars on GitHub. Gatsby is described as "a free and open source framework based on React that helps developers build blazing fast **website** and **apps**." The rich ecosystem of plugins allow you to pull your data from an endless array of options, including headless CMSs, APIs, databases, or local markdown files. It also provides out-of-the-box code splitting, link pre-fetching, and other performance optimizations.

However, having spent a fair bit of time with Vue recently, and really liking it's HTML-like templates, I decided to see if there was a suitable competitor in the Vue world. My experience with both Vue and React made me realize that Vue is much more approachable for smaller projects like this, especially if a new developer joins the team later on.

### Gridsome

While Gridsome is clearly the newcomer to the scene, shown by its only  `2000` stars on GitHub, it is still only in beta. Gridsome is clearly influenced by Gatsby, offering much of the same functionality, but built around the Vue templating framework. Similar to Gatsby, Gridsome gathers data from a plethora of sources, provides the data to the app through a GraphQL layer, and can deploy the compiled static files to any static web host.

> I actually did start a Gatsby site from a template project but soon got frustrated by several things I knew were easier in Vue, such as styling with CSS (in the same file), JSX markup (vs. customized HTML), and numerous other small complaints.

## On To the App

Finally having settled on a static site generator (and having renamed the old repository), I created a new GitLab repository and began searching for a good project template. I quickly found an attractive [blogging template](https://github.com/gridsome/gridsome-starter-blog) (one of the few available that supported tags) and generated a project using the [Gridsome CLI](https://gridsome.org/docs/gridsome-cli/).

As a side note, the template had several features that gathered my interest initially:

- **CSS variables** - Used CSS variables (rather than SCSS variables) for their better performance and ability to be changed on the fly with JS or breakpoints, etc.
- **Google Lighthouse** - Perfect score on Google Lighthouse (site performance auditor)
- **Markdown blog** - Existing examples for a Markdown blog _with_ tag support

### Creating the Project

Gridsome provides a CLI (command line interface) with a single command: `create`. The CLI accepts an optional template project parameter, which can point to a starter template GitHub repository.

```bash
# Globally install Gridsome CLI
npm install --global @gridsome/cli

# Create the site from a template and get it running
gridsome create family-merchant-site https://github.com/gridsome/gridsome-starter-blog.git
cd family-merchant-site
npm run develop
```

### Configuring Webpack

Before I started updating anything, I quickly overhauled the Webpack configuration with some of my typical preferences. I almost always leverage Webpack "aliases" to provide cleaner import paths and avoid relative imports. In the example below, both changes are relatively minor but add up over time. Using pseudo-absolute paths for imports means no worries about breaking imports when moving files around, as the paths are handled by Webpack

```javascript
// Good
import Footer from "@components/Footer";
// Bad
import Footer from "../../components/Footer";

// Good
import Footer from "@components/Footer";
// Bad
import Footer from "@components/Footer.vue";
```

Gridsome provides a `chainWebpack` hook in the `gridsome.config.js` file to manage the Webpack configuration using the `chain-webpack` library. An example of customizing (not overwriting) the Webpack import path resolution is seen below:

```javascript
chainWebpack: (config) => {
  const webpackAliases = {
    "@assets": path.join(currentDir, "assets"),
    "@components": path.join(currentDir, "components"),
  };

  // Configure webpack aliases
  for (key of Object.keys(webpackAliases)) {
    config.resolve.alias.set(key, webpackAliases[key]);
  };
```

> I also had to add Vuetify as an external Node library when being rendered on the server, to avoid some strange conflicts.

## Customizing the Template

After verifying that everything worked properly and browsing the site locally, I started ~~customizing~~ completely overhauling the site to fit my theme. The primary structure remained the same but I replaced all styles, images, and content initially.

> Be wary about trying to do too much at one time when initially creating your site. It is often better to get a solid but minimal site completed and deployed before spending too much time rounding it out. This ensures that the deployment process is sound and avoids never actually getting around to deploying the site.

### Updating Content

The header and footer were some of the first components I overhauled, as I wanted to focus on the Home page when developing a feel for what the eventual theme would be. A brief bit of work led to a full screen "hero" image depicting concept art for the game, overlaid with a basic title and quote. A similar full screen image is displayed predominantly on the custom 404 page, as a lone adventurer shares your sorrow at the missing content.

I added some "standard" (at least for my projects) source folders and spent a bit of time refactoring the existing components. Most pages shared a page title and a main column, so I extracted these to their own components and updated the pages to use the new components.

At some point I also imported the [Vuetify](https://vuetifyjs.com) component library, designed around the Google Material Design principles. I have used the library extensively in other project and am blown away by the excellent components and customization the library provides. While it is easy to wind up with a very Google-esque site when using Material Design libraries, Vuetify provides some basic components that drastically improve development (cards, grid system, utility classes, etc).

### Directory Structure

This represents the basic new directory structure for the app, including all key folders and files.

```
content/
  - posts/
src
  - assets/
    - icons/
      - favicon.ico
    - images/
      - hero.png
    - styles/
      - _breakpoints.scss
      - styles.scss
  - components/
    - Blog/
      - PostCard.vue
      - PostTags.vue
    - Layout/
      - TheAppHeader.vue
    - Title.vue
    - PageLayout.vue
  - data/
  - layouts/
    - Default.vue
  - pages/
    - 404.vue
    - Index.vue
    - Blog.vue
  - templates/
    - Post.vue
  - index.html
  - main.js
static/
  - images/
    - 2019-03-11-cover-image.png
  - _redirects
package.json
gridsome.config.js
README.md
```

### Overhauling the Blog

The blog was one the major attractions to the starter template, due to its minimal yet beautiful design. I removed all of the starter blog posts and created a simple replacement post, listing my goals for the project and welcoming the reader to follow along the journey. I did refactor the blog list and post components into a more maintainable structure and updated several styles, but largely kept to the provided template.

The template provided a simple cover image pattern through an image provided in the blog post frontmatter, displayed in both the blog list preview and blog post details. Speaking of frontmatter, Markdown offers a fantastic approach to storing additional data (besides_ the main content) within the file itself. Frontmatter is stored in a YAML format at the top of a Markdown file, between two lines of hyphens (`---`).

This frontmatter (included in a normal blog post) defines the post's title, author, cover image, and other attributes.

```markdown
---
title: Welcome to Family Merchant!
published: true
author: Kendall Roth
date: 2019-03-11
cover_image: /2019-03-11-synty-adventure.png
description: This blog will chronicle my journey in game development..
tags: ["Game", "Project"]
...
---

## Welcome!

This blog will chronicle my journey in game development...
```

The Gridsome `source-filesystem` plugin handles reading the `content/posts` directory contents and creating a route for each file. The files are passed to the `remark` transformer to be parsed from Markdown to HTML, with the frontmatter being added to the GraphQL node alongside the content.

### Displaying the Blog

Both "pages" and "templates" have access to the GraphQL query component `<page-query>`, enabling them to query the Gridsome GraphQL schema for the required page data. The query can contain lists (Blog list page) or a single node (Post page), and is included as a top-level section in the SFC (single file component). Since the `remark` transformer included the frontmatter data in the created GraphQL node, it is accessible in the GraphQL query along with the content.

The following query gets all posts (not paginated) and determines which data should be provided to the Vue component. Note that GraphQL provides **only** the data included the query; any attributes not specified will not be provided! Fields on the query can provide arguments to customize the return data, such as resizing the cover image and providing a blur while loading, or formatting the date published.

```graphql
<page-query>
{
  posts: allPost {
    edges {
      node {
        id
        coverImage(width: 800, blur: 10)
        content  # Actual Markdown content
        date (format: "MMMM D, YYYY")
        title
        timeToRead
        tags {
          id
          title
          path
        }
      }
    }
  }
}
</page-query>
```

The queried data is then accessible in the component as the `$page` key (ie. `$page.post.timeToRead`).

> Since the post content is represented in HTML (transformed from Markdown), it can only be displayed with the `v-html="$page.post.content` attribute on a component.

### Handling Tags

The template came with built-in tag support, using a Gridsome feature called "taxonomies." When defining the blog filesystem parsing plugin, a "reference" (`refs`) can be defined on the file type to designate a related collection (along with the route it should be visible on).

```javascript
plugins: [
  {
    // Create posts from markdown files
    use: "@gridsome/source-filesystem",
    options: {
      typeName: "Post",
      path: "content/posts/*.md",
      route: "/blog/:slug",
      refs: {
        // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
        tags: {
          typeName: "Tag",
          route: "/blog/tag/:id",
          create: true,
        },
      },
    },
  },
},
```

## Conclusion

A lengthy post indeed, but covering one of the biggest topics in developing a game website...actually getting started! The next post in the series will look at deploying the site to a static site CDN.
