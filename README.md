# Family Merchant Site 

> A simple, hackable & minimalistic starter for Gridsome that uses Markdown for content.

**Note**: This starter uses [Gridsome](https://www.gridsome.org) and the [Netlify CMS](https://www.netlifycms.com/).

It follows the [JAMstack architecture](https://jamstack.org/) by using Git as a single source of truth, and [Netlify](https://www.netlify.com/) for continuous deployment and CDN distribution.

## Features
- _N/A_

## Prerequisites

- Node (use v8.2.0 or higher)
- [Gridsome](https://www.gridsome.org/docs/) (global installation)

## Development

```
$ git clone https://gitlab.com/unavia-games/family-merchant-site.git
$ cd family-merchant-site
$ gridsome develop
```

To test the Netlify CMS locally, you'll need run a production build of the site:

```
$ gridsome build
```

### Setting up the CMS
Follow the [Netlify CMS Quick Start Guide](https://www.netlifycms.org/docs/quick-start/#authentication) to set up authentication, and hosting.

### Notes

- There is an issue ([GitHub - Gridsome #285](https://github.com/gridsome/gridsome/issues/285)) with Gridsome frontmatter parsing that requires image paths to start with either `/` or `./`. Since the Netlify CMS does not recognize either of these paths, but instead uses no prepended path, it is not recommended to use the CMS for displaying images. Rather, any images like this should be added manually in the Markdown file. The image widget has been disabled to prevent saving over and erasing existing images.
