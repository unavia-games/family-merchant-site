# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Add common "layout" components (!4)
- Improve Google Lighthouse score (!3)
- Add Netlify CMS for content management (!2)
- Add basic Vue project and site content (!1)
