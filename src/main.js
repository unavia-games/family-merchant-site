// 3rd Party
import Vuetify from "vuetify";

// Components
import PageLayout from "@components/PageLayout";
import Title from "@components/Title";
import DefaultLayout from "@layouts/Default";

// Styles
import "vuetify/dist/vuetify.min.css";
import "@styles/index.scss";

// The Client API can be used here. Learn more: gridsome.org/docs/client-api
export default function(Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);

  // Common Vue components
  Vue.component("PageLayout", PageLayout);
  Vue.component("Title", Title);

  // Configure plugins
  Vue.use(Vuetify, {
    theme: {
      primary: "#A63726",
    },
  });
}
