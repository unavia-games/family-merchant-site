---
title: About Family Merchant
---

> The To Raise a Family game universe chronicles the life of various individuals as they struggle to make ends meet while starting a family. As different challenges get in the way of providing for their family, how far will they go to continue providing for their family?

## Family Merchant

To Raise a Family: Family Merchant places the character in the shoes of a young newly-married lad setting out on his own to begin his life and family. With only two initial plots of land to his name, the young merchant must seek to establish himself as family provider through managing a small market. From gathering resources to scurrying around on market days, he must be able to wisely manage and expand his market. While his wife manages the household, he must also take care to remain involved in his family’s daily life, lest he end like he began, alone and with nothing of true value.

## Objectives

- **Exploration** - Explore a small open world
- **Survival** - Manage your basic needs and gather resources
- **Simulation** - Manage a market to provide for your family
- **RPG** - Expand your skill set and respect from others

## World Setting
The game is set within a peaceful seaside valley surrounded by rolling hills. The valley is littered with a sparse cover of trees with several meadows and denser forests. Small streams flow through the landscape, gathering in pools frequented by the local wildlife. The abundant plant life sustains a small game population, with wolves as the only occasional predators. Several minor cave systems contain a variety of mineral resources and also some lurking inhabitants.

One end of the valley contains a large city (unplayable) controlling the surrounding province, while the other end contains a small village (playable). Midway between the two ends lies the abandoned market outpost that the main character will inherit and be tasked with managing. Several other locations of interest dot the landscape for the player to discover.
