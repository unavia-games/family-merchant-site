module.exports = {
  env: {
    node: true,
    es6: true,
  },
  extends: ["plugin:prettier/recommended"],
  parser: "vue-eslint-parser",
  plugins: ["gridsome"],
  rules: {
    "comma-dangle": "always-multiline",
    "gridsome/format-query-block": "warn",
  },
};
