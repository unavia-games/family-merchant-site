// Project configuration and plugin options

const path = require("path");
const nodeExternals = require("webpack-node-externals");

const currentDir = path.join(__dirname, "src");

const webpackAliases = {
  "@assets": path.join(currentDir, "assets"),
  "@components": path.join(currentDir, "components"),
  "@data": path.join(currentDir, "data"),
  "@icons": path.join(currentDir, "assets", "icons"),
  "@images": path.join(currentDir, "assets", "images"),
  "@layouts": path.join(currentDir, "layouts"),
  "@pages": path.join(currentDir, "pages"),
  "@styles": path.join(currentDir, "assets", "styles"),
  "@templates": path.join(currentDir, "templates"),
};

const webpackExtensions = ["vue", "js", "json", "scss"];

module.exports = {
  siteName: "Family Merchant",
  siteDescription: "Information website for the Family Merchant game",
  icon: "src/assets/icons/favicon.png",
  plugins: [
    {
      // Create posts from markdown files
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Post",
        path: "content/posts/*.md",
        route: "/blog/:slug",
        refs: {
          // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
          tags: {
            typeName: "Tag",
            route: "/blog/tag/:id",
            create: true,
          },
        },
      },
    },
    {
      use: "gridsome-plugin-netlify-cms",
      options: {
        // Source path to Netlify CMS configuration
        configPath: "static/admin/config.yml",
        htmlTitle: "CMS - Family Merchant",
        // Site path to Netlify CMS
        publicPath: "/admin",
      },
    },
  ],

  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: "_blank",
      externalLinksRel: ["nofollow", "noopener", "noreferrer"],
      anchorClassName: "icon icon-link",
      plugins: ["@gridsome/remark-prismjs"],
    },
  },

  chainWebpack: (config, { isServer }) => {
    // Fix Vuetify CSS import (https://github.com/gridsome/gridsome/issues/43)
    if (isServer) {
      config.externals([
        nodeExternals({
          whitelist: [/^vuetify/],
        }),
      ]);
    }

    // Parsing markdown and frontmatter (for data/pages)
    config.module
      .rule("markdown")
      .test(/\.md$/)
      .use("frontmatter-markdown-loader")
      .loader("frontmatter-markdown-loader")
      .end();

    // Configure webpack aliases
    for (key of Object.keys(webpackAliases)) {
      config.resolve.alias.set(key, webpackAliases[key]);
    }

    // Configure webpack file extensions
    for (key of webpackExtensions) {
      config.resolve.extensions.add(key);
    }
  },
};
